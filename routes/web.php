<?php

use App\Http\Controllers\AppointmentController;
use App\Http\Controllers\DashBoardController;
use App\Http\Controllers\TimeController;
use App\Http\Controllers\DoctorController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ViewController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

Route::get('/', [ViewController::class, 'index']);
Route::get('/new-appointment/{doctorId}/{date}', [ViewController::class, 'show'])->name('create.appointment');

Route::get('/home', [HomeController::class, 'index'])->name('home');

Auth::routes();

Route::post('/book/appointment', [ViewController::class, 'store'])->name('book.appointment');

// Doctor Routes
Route::group(['middleware' => ['auth', 'doctor']], function () {
    Route::resource('appointment', AppointmentController::class);
    Route::post('/appointment/check', [AppointmentController::class, 'check'])->name('appointment.check');
    Route::post('/appointment/update', [AppointmentController::class, 'updateTime'])->name('update');
    Route::resource('timeslot', TimeController::class);
    Route::get('/timeslot/edit/{id}', [TimeController::class, 'show'])->name('timeslot.edit');
    Route::put('/timeslot/update/{id}', [TimeController::class, 'update'])->name('timeslot.update');
});
