<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Appointment;
use App\Models\Time;
use App\Models\User;
use App\Models\Booking;
use App\Models\TimeSlot;
use Illuminate\Support\Facades\Auth;

class ViewController extends Controller
{
    public function index(Request $request)
    {
        if(auth()->user()) {
            if(auth()->user()->role_id == 1) {
                return redirect(route('home'));
            }
        }
        
        $formatDate = date('Y-m-d');
        $formatDatetime = date('Y-m-d H:i:s');
        // If there is set date, find the doctors
        if (request('date')) {

            $date = request('date');
            $datetime = $date;

            if(request('date') == date('Y-m-d')) {
                $datetime = request('date').' '.date('H:i:s');
            }

            $formatDate = date('Y-m-d', strtotime($date));
            $formatDatetime = date('Y-m-d H:i:s', strtotime($datetime));
        }
        
        $doctors = User::with('availableSlots')
                        ->whereHas('availableSlots', function ($query) use ($formatDate, $formatDatetime) {
                            $query->where('date_start', $formatDate)
                                ->where('datetime_start', '>=', $formatDatetime);
                        })
                        ->get();

        return view('welcome', compact('doctors', 'formatDate'));
    }

    public function show($doctorId, $date)
    {
        $datetime = $date;
        if($date == date('Y-m-d')) {
            $datetime = $date.' '.date('H:i:s');
        }

        $datetime = date('Y-m-d H:i:s', strtotime($datetime));

        $availableSlots = TimeSlot::with('appointment')
                                ->where('user_id', $doctorId)
                                ->where('date_start', $date)
                                ->where('datetime_start', '>=', $datetime)
                                ->get();

        $user = User::where('id', $doctorId)->first();
        $doctor_id = $doctorId;

        return view('appointment', compact('availableSlots', 'date', 'user', 'doctor_id'));
    }

    public function store(Request $request)
    {
        $request->validate(['slot' => 'required']);

        $doctorId = $request->doctorId;
        $slot = $request->slot;
        $date = $request->date;
        
        Appointment::create([
            'user_id' => auth()->user()->id,
            'doctor_id' => $doctorId,
            'time_slot_id' => $slot,
            'status' => Appointment::STATUS_CONFIRM,
        ]);
        $doctor = User::where('id', $doctorId)->first();

        $slot = TimeSlot::find($slot);

        $message = 'Your appointment was booked for ' . $date . ' at ' . date("h.i A", strtotime($slot->time_start)) . ' - '. date("h.i A", strtotime($slot->time_end)) .' with ' . $doctor->name . '.';
        
        return redirect()->back()->with('message', $message);
    }
}
