<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TimeSlot;
use DateInterval;
use DateTime;
use Laravel\Ui\Presets\React;

class TimeController extends Controller
{
    public function __construct() {}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $timeSlots = TimeSlot::where('user_id', auth()->user()->id)
                                ->where('datetime_start', '>=', date('Y-m-d H:i:s'))
                                ->get();

        return view('admin.timeslot.index', compact('timeSlots'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.timeslot.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateStore($request);
        
        $dateStart = $request->date_start;
        $dateEnd = $request->date_end;
        $timeStart = $request->time_start;
        $timeEnd = $request->time_end;

        $timeRange = $request->time_range;
        $rangeType = $request->range_type;

        $datetimeStart = $dateStart.' '.$timeStart;
        $datetimeEnd = $dateEnd.' '.$timeEnd;

        $duration = $timeRange; // how much the is the duration of a time slot
        $start    = $datetimeStart; // start time
        $end      = $datetimeEnd; // end time
        $availableSlots = $this->availableSlots($duration, $start, $end);
        
        foreach ($availableSlots as $slot) {
            $timeSlotModel = new TimeSlot();
            $timeSlotModel->user_id = $request->user_id;
            $timeSlotModel->slot_type = $rangeType;
            $timeSlotModel->slot_range = $timeRange;
            $timeSlotModel->date_start = $slot['date_start'];
            $timeSlotModel->time_start = $slot['time_start'];
            $timeSlotModel->datetime_start = $slot['date_start'].' '.$slot['time_start'];
            $timeSlotModel->date_end = $slot['date_end'];
            $timeSlotModel->time_end = $slot['time_end'];
            $timeSlotModel->datetime_end = $slot['date_end'].' '.$slot['time_end'];

            $timeSlotModel->save();
        }

        $response = $this->message(200, 'STORE_SUCCESS', 'TimeSlot Created Successful.');

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $timeslot = TimeSlot::find($id);
        
        return view('admin.timeslot.edit', compact('timeslot'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $timeslot = TimeSlot::find($id);
        $timeslot->update([
            "user_id" => $request->user_id,
            "date_start" => $request->date_start,
            "date_end" => $request->date_end,
            "slot_range" => $request->time_range,
            "slot_type" => $request->range_type,
            "time_start" => $request->time_start,
            "time_end" => $request->time_end
        ]);
        
        $response = $this->message(200, 'UPDATE_SUCCESS', 'TimeSlot Updated Successful.');

        return response()->json($response);
    }

    // Check specific date for appointment time
    public function check(Request $request)
    {
        $timeslotId = $request->timeslot_id;
        $timeslot = TimeSlot::where('id', $timeslotId)->where('user_id', auth()->user()->id)->first();
        
        return view('admin.timeslot.edit', compact('timeslot'));
    }

    public function validateStore($request)
    {
        return  $this->validate($request, [
            'user_id' => 'required',
            'date_start' => 'required',
            'date_end' => 'required',
            'time_range' => 'required',
            'range_type' => 'required',
            'time_start' => 'required',
            'time_end' => 'required',
        ]);
    }

    public function availableSlots($duration, $start, $end)
    {
        $start         = new DateTime($start);
        $end           = new DateTime($end);
        $interval      = new DateInterval("PT" . $duration . "M");
        $periods = array();
    
        for ($intStart = $start; $intStart < $end; $intStart->add($interval)) {
            $endPeriod = clone $intStart;
            $endPeriod->add($interval);
    
            $periods[] = [
                "date_start" => $intStart->format('Y-m-d'),
                "time_start" => $intStart->format('H:i:s'),
                "date_end" => $endPeriod->format('Y-m-d'),
                "time_end" => $endPeriod->format('H:i:s')
            ];
        }
    
        return $periods;
    }
}