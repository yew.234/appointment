<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TimeSlot extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'slot_type',
        'slot_range',
        'date_start',
        'time_start',
        'datetime_start',
        'date_end',
        'time_end',
        'datetime_end'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function getDatetimeStartAttribute()
    {
        return "{$this->date_start} {$this->time_start}";
    }

    public function getDatetimeEndAttribute()
    {
        return "{$this->date_end} {$this->time_end}";
    }

    public function user()
    {
        return $this->hasMany(User::class, 'id', 'user_id');
    }

    public function appointment()
    {
        return $this->hasOne(Appointment::class, 'time_slot_id', 'id');
    }
}
