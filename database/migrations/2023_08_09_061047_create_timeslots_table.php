<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

Class CreateTimeSlotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('time_slots', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('slot_type');
            $table->string('slot_range');
            $table->date('date_start');
            $table->time('time_start');
            $table->dateTime('datetime_start');
            $table->date('date_end');
            $table->time('time_end');
            $table->datetime('datetime_end');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timeslots');
    }
};
