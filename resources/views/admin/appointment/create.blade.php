@extends('admin.layouts.master')

@section('content')

    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="ik ik-command bg-blue"></i>
                    <div class="d-inline">
                        <h5>Appointment TimeSlots</h5>
                        <span>Add your available time for appointments</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <nav class="breadcrumb-container" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{ route('home') }}"><i class="ik ik-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Appointment TimeSlots</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Create</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-lg-10">
            <div class="alert-msg alert bg-success alert-success text-white text-center d-none" role="alert">
                <span id="message"></span>
            </div>
            
            <div class="card">
                <div class="card-header">
                    <h3>New TimeSlot</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-2">
                            <label>Date</label>
                        </div>
                        <div class="col-lg-2">
                            <input id="date_start" type="date" name="date_start" class="form-control @error('date_start') is-invalid @enderror"
                                value="{{ old('date_start') }}">
                            @error('date_start')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-lg-1 text-center">
                            <label>to</label>
                        </div>
                        <div class="col-lg-2">
                            <input id="date_end" type="date" name="date_end" class="form-control @error('date_end') is-invalid @enderror"
                                value="{{ old('date_end') }}">
                            @error('date_end')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-lg-2">
                            <label>Time Range</label>
                        </div>
                        <div class="col-lg-2">
                            <input id="time_start" type="time" name="time_start" class="form-control @error('time_start') is-invalid @enderror"
                                value="{{ old('time_start') }}">
                            @error('time_start')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-lg-1 text-center">
                            <label>to</label>
                        </div>
                        <div class="col-lg-2">
                            <input id="time_end" type="time" name="time_end" class="form-control @error('time_end') is-invalid @enderror"
                                value="{{ old('time_end') }}">
                            @error('time_end')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-lg-2">
                            <label>Time Slot Increment</label>
                        </div>
                        <div class="col-lg-1">
                            <label>Every</label>
                        </div>
                        <div class="col-lg-1">
                            <input id="time_range" type="number" name="time_range" class="form-control @error('time_range') is-invalid @enderror"
                                value="20" min="20" max="60">
                            @error('time_range')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-lg-1">
                            <label>minutes</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <button id="submit" type="button" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('#submit').click(function() {
            let date_start = $('#date_start').val();
            let date_end = $('#date_end').val();
            let time_start = $('#time_start').val();
            let time_end = $('#time_end').val();
            let time_range = $('#time_range').val();

            let data = {
                "user_id" : {{ auth()->user()->id }},
                "date_start" : date_start,
                "date_end": date_end,
                "time_range": time_range,
                "range_type": "minutes",
                "time_start": time_start,
                "time_end": time_end
            }
            console.log(data);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "{{ route('time.create') }}",
                data: data,
                dataType:"json",
                success: function(response){
                    if(response.status = "200")
                    {
                        $('#message').html(response.message);
                        $('.alert-msg').removeClass('d-none');
                    }
                }
            });
        });
    </script>
@endsection
