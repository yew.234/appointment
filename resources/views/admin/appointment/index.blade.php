@extends('admin.layouts.master')

@section('content')

    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">


                <div class="page-header-title">
                    <i class="ik ik-command bg-blue"></i>
                    <div class="d-inline">
                        <h5>Appointment</h5>
                        <span>Available Appointment</span>

                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <nav class="breadcrumb-container" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{ route('home') }}"><i class="ik ik-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Appointment</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Available Appointment</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-lg-10">
            @if (Session::has('message'))
                <div class="alert bg-success alert-success text-white" role="alert">
                    {{ Session::get('message') }}
                </div>
            @endif
            @if (Session::has('errMessage'))
                <div class="alert bg-danger alert-success text-white" role="alert">
                    {{ Session::get('errMessage') }}
                </div>
            @endif
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger">
                    {{ $error }}

                </div>
            @endforeach

          
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Date</th>
                        <th scope="col">Time</th>
                        <th scope="col">Doctor</th>
                        <th scope="col">Patient</th>
                        <th scope="col">View & Update</th>
                    </tr>
                </thead>
                <tbody>
                    @if ($myAppointments->count() > 0)
                        @foreach ($myAppointments as $index => $appointment)
                            <tr>
                                <td>{{ ++$index }}</td>
                                <td>{{ date("Y-m-d", strtotime($appointment->timeslot->date_start)) }}</td>
                                <td>{{ date("h:i A", strtotime($appointment->timeslot->time_start)).' - '.date("h:i A", strtotime($appointment->timeslot->time_end)) }}</td>
                                <td>{{ $appointment->doctor->name }}</td>
                                <td>{{ $appointment->patient->name }}</td>
                                <td>
                                    <form action="{{ route('appointment.check') }}" method="post">@csrf
                                        <input type="hidden" name="date" value="{{ $appointment->date }}">
                                        <button type="submit" class="btn btn-primary">View & Update</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6">There is no Appointment for now.</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>

@endsection
