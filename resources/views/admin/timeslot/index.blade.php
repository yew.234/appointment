@extends('admin.layouts.master')

@section('content')

    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">


                <div class="page-header-title">
                    <i class="ik ik-command bg-blue"></i>
                    <div class="d-inline">
                        <h5>Check TimeSlots</h5>
                        <span>Available TimeSlots</span>

                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <nav class="breadcrumb-container" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{ route('home') }}"><i class="ik ik-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">TimeSlots</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Check</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-lg-10">
            @if (Session::has('message'))
                <div class="alert bg-success alert-success text-white" role="alert">
                    {{ Session::get('message') }}
                </div>
            @endif
            @if (Session::has('errMessage'))
                <div class="alert bg-danger alert-success text-white" role="alert">
                    {{ Session::get('errMessage') }}
                </div>
            @endif
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger">
                    {{ $error }}

                </div>
            @endforeach

          
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Date</th>
                        <th scope="col">Time</th>
                        <th scope="col">Duration</th>
                        <th scope="col">View & Update</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($timeSlots as $index => $slot)
                        <tr>
                            <td>{{ ++$index }}</td>
                            <td>{{ date("Y-m-d", strtotime($slot->date_start)) }}</td>
                            <td>{{ date("h.i A", strtotime($slot->time_start)) }} - {{ date("h.i A", strtotime($slot->time_end)) }}</td>
                            <td>{{ $slot->slot_range.' '.$slot->slot_type }}</td>
                            <td>
                                <button type="button" class="btn btn-primary" onclick="view('{{ $slot->id }}')">View and Update</button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>

    <script>
        function view(id) {
            window.location.href = "/timeslot/edit/" + id;
        }
    </script>
@endsection
