## Getting Started
# Appointment Application
- Create an Appointment Application Portal by using Laravel 9 and designing into User(Doctor, Patient) and TimeSlots Available.
- There will a Patient Booking Webpage and Doctor Web Portal.
- First, Doctor have to create own available timeslot on Portal after login.
- Then, Patient also require to register as an user to book appointment on related doctor available timeslot.
- Doctor's TimeSlot able to create by input as below :
    - Date: From - To
    - TimeRange: Start - End
    - TimeSlot Increment: Every [?] Minutes
- Doctor able to view and update specific TimeSlot as well.
- Every TimeSlot only able to select by 1 Patient

# Application Credential
- Doctor 
    - User : doctorlim@gmail.com
    - Password : password

## Application Setup / Installation
1. run : copy .env.example .env
2. run : composer install
3. run : php artisan key:generate
4. run : npm install
5. run : npm run dev
6. run : php artisan migrate
7. run : php artisan db:seed
8. run : php artisan serve

